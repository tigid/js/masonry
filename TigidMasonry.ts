type TOptions = {
  columns: number;
  gap: number;
  gapX?: number;
  gapY?: number;
}

class TigidMasonry {
  private element: HTMLDivElement;
  private options: TOptions;
  private items: HTMLElement[];
  private columnsY: number[];

  constructor(element: HTMLDivElement, options: TOptions) {
    this.element = element;
    this.options = options;
    this.init();
    this.events();
  }

  private init() {
    this.items = Array.from(this.element.children) as HTMLElement[];

    if(!this.element.style.position){
      this.element.style.position = 'relative';
    }

  }

  private events() {
    window.addEventListener('resize', () => this.reflow());
    window.addEventListener('DOMContentLoaded', () => this.reflow());
  }

  public reflow() {
    this.columnsY = Array(this.options.columns).fill(0);

    const gapX = this.options.gapX || this.options.gap || 0;
    const gapY = this.options.gapY || this.options.gap || 0;
    const columns = this.options.columns;

    const columnWidth = (this.element.clientWidth - gapX * (columns - 1)) / columns;

    this.items.forEach((item, i) => {
      const column = this.getNextColumn();
      const row = Math.floor(i / columns);
      const x = column * (columnWidth + gapX);
      const y = this.columnsY[column];

      item.style.position = `absolute`;
      item.style.width = `${columnWidth}px`;
      item.style.left = `${x}px`;
      item.style.top = `${y}px`;

      this.columnsY[column] = this.columnsY[column] + item.clientHeight + gapY;
    })
  }

  private getNextColumn() {
    const minValue = Math.min(...this.columnsY);
    return this.columnsY.findIndex(item => item === minValue) || 0;
  }
}