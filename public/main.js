function initMasonry() {
  const elem = document.getElementById('masonryContainer');
  const masonry = new TigidMasonry(elem, {
    columns: 1,
    gapX: 20,
    gapY: 20,
  });
}

initMasonry();